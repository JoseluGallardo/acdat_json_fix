package com.jsw.acdat_json_fix;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class Ejercicio3Activity extends AppCompatActivity {

    final String WEB = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    @BindView(R.id.edtxt_eur)
    EditText eurBox;
    @BindView(R.id.edtxt_usd)
    EditText usdBox;
    @BindView(R.id.rbtn_eurAUsd)
    RadioButton eurAusd;
    AsyncHttpClient mClient;
    Context mContext;
    private Conversor convierte;
    private Double resultado;
    private double value = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);
        ButterKnife.bind(this);
        this.mContext = this;
        this.convierte = new Conversor();
        this.convierte.download();
    }

    public void convertir(View v) {

        if (eurAusd.isChecked()) { //Si está checkeado
            if (eurBox.getText().toString() != "") { //y tu texti es distinto de vacio
                try {
                    resultado = convierte.EurAUsd(Double.parseDouble(eurBox.getText().toString())); //Guarda en Resultado el double devuelto en el método
                    usdBox.setText(resultado.toString()); //Y actualiza el texto
                } catch (Exception ex) {
                }
            }
        } else if (usdBox.getText().toString() != "") { //Si no está marcado, y los dolares están distintos de 0
            try {
                resultado = convierte.UsdAEur(Double.parseDouble(usdBox.getText().toString())); //Lo mismo
                eurBox.setText(resultado.toString());
            } catch (Exception ex) {
            }
        }
    }


    final class Conversor {

        public double EurAUsd(double euros) {
            return euros / value;
        }

        public double UsdAEur(double usd) {
            return usd * value;
        }

        private void download() {
            final ProgressDialog progreso = new ProgressDialog(mContext);
            mClient = new AsyncHttpClient();

            final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "chnge.txt");
            RequestHandle requestHandle = mClient.get(WEB, new FileAsyncHttpResponseHandler(miFichero) {

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, File file) {
                    progreso.dismiss();
                    try {
                        //Se almacena en una variable de tipo 'double' el valor de cambio de euros a dólares
                        value = Utils.getUSD(miFichero);
                    } catch (IOException | XmlPullParserException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStart() {
                    super.onStart();
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Obteniendo valor de cambio . . .");
                    progreso.setCancelable(false);
                    progreso.show();
                }
            });
            requestHandle.setTag("");
        }
    }
}
