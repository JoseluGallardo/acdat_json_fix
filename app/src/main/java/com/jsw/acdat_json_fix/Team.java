package com.jsw.acdat_json_fix;
/**
 * Created by Joselu on 7/1/17.
 */

public class Team {
    String image;
    String name;
    String score;

    public Team(String name, String score) {
        this.name = name;
        this.score = score;
        this.image = "http://i.nflcdn.com/static/site/7.4/img/teams/" + name + "/" + name + "_logo-80x90.gif";
    }
}
