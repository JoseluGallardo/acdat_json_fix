package com.jsw.acdat_json_fix;
/**
 * Created by Joselu on 7/1/17.
 */

public class Score {
    Team home;
    Team visitor;

    public Score(Team home, Team visitor) {
        this.home = home;
        this.visitor = visitor;
    }
}
