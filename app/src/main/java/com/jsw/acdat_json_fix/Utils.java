package com.jsw.acdat_json_fix;
import android.os.Environment;
import android.util.Xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Joselu on 4/1/17.
 */

public class Utils {
    public static Data getWeatherInfo(JSONObject jsonObj) throws JSONException {
        Data estado = new Data();

        //Estado del cielo
        JSONArray jsonWeather = jsonObj.getJSONArray("weather");
        JSONObject wTmp = jsonWeather.getJSONObject(0);
        estado.Tiempo = wTmp.getString("main");
        estado.Cloud = wTmp.getString("description");
        estado.Icon = wTmp.getString("icon");


        //Info
        JSONObject mTmp = jsonObj.getJSONObject("main");
        estado.Grados = mTmp.getString("temp");
        estado.Humidity = mTmp.getString("humidity");
        estado.Preassure = mTmp.getString("pressure");

        //Sistema
        JSONObject sTmp = jsonObj.getJSONObject("sys");
        estado.City = jsonObj.getString("name");
        estado.Sunrise = sTmp.getString("sunrise");
        estado.Sunset = sTmp.getString("sunset");

        return estado;
    }

    public static ArrayList<CityData> getCityInfo(JSONObject response) throws JSONException {
        ArrayList<CityData> preds = new ArrayList<>();


        JSONArray arrayDatos = response.getJSONArray("list");

        for (int i = 0; i < arrayDatos.length(); i++) {
            CityData prd = new CityData();

            JSONObject datos = arrayDatos.getJSONObject(i);
            JSONObject temperatura = datos.getJSONObject("temp");

            prd.maxTemp = temperatura.getDouble("max");
            prd.maxTemp = temperatura.getDouble("min");
            prd.preassure = datos.getDouble("pressure");
            prd.date = new SimpleDateFormat("dd/MM/yyyy").format(new Date(datos.getLong("dt") * 1000));

            preds.add(prd);
        }
        return preds;
    }

    /*public static CiudadCityData analizarCiudadCityDataes(JSONObject response) throws JSONException {
        CiudadCityData cp = new CiudadCityData();
        JSONObject ciudad = response.getJSONObject("city");
        cp.nombre = ciudad.getString("name");
        cp.pais = ciudad.getString("country");

        return cp;
    }*/

    public static void almacenarJSON(ArrayList<CityData> cityList, String fichero_json) throws IOException {
        OutputStreamWriter out;
        File miFichero;
        File tarjeta;
        String contenido;

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();

        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero_json);
        out = new FileWriter(miFichero);

        contenido = gson.toJson(cityList);
        out.write(contenido);

        out.close();
    }

    public static void almacenarXML(ArrayList<CityData> listaCityDataes, String fichero_xml, String name) throws IOException {
        File miFichero;
        File tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero_xml);

        FileOutputStream escritorXML = new FileOutputStream(miFichero);
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(escritorXML, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        serializer.startTag(null, "predicciones");
        serializer.startTag(null, "ciudad");
        serializer.attribute(null, "pais", "españa");
        serializer.text(name);
        serializer.endTag(null, "ciudad");
        for (CityData prediccion : listaCityDataes) {
            serializer.startTag(null, "prediccion");

            serializer.startTag(null, "date");
            serializer.text(prediccion.date);
            serializer.endTag(null, "date");

            serializer.startTag(null, "tmpMin");
            serializer.attribute(null, "und", "Celsius_ºC");
            serializer.text(String.valueOf(prediccion.minTemp));
            serializer.endTag(null, "tmpMin");

            serializer.startTag(null, "tmpMax");
            serializer.attribute(null, "und", "Celsius_ºC");
            serializer.text(String.valueOf(prediccion.maxTemp));
            serializer.endTag(null, "tmpMax");

            serializer.startTag(null, "preassure");
            serializer.attribute(null, "und", "Hectopascal_hPa");
            serializer.text(String.valueOf(prediccion.preassure));
            serializer.endTag(null, "preassure");

            serializer.endTag(null, "prediccion");
        }
        serializer.endTag(null, "predicciones");
        serializer.endDocument();
        serializer.flush();
        escritorXML.close();
    }

    public static double getUSD(File file) throws IOException, XmlPullParserException {
        double cambio = 0;

        int eventType;
        XmlPullParser xrp = Xml.newPullParser();
        xrp.setInput(new FileReader(file));
        eventType = xrp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equalsIgnoreCase("Cube")) {
                        if (xrp.getAttributeCount() == 2) {
                            if (xrp.getAttributeValue(0).equals("USD")) {
                                cambio = Double.parseDouble(xrp.getAttributeValue(1));
                            }
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    break;
            }
            eventType = xrp.next();
        }
        return cambio;
    }

    public static double getEUR(File file) throws IOException, XmlPullParserException {
        double cambio = 0;

        int eventType;
        XmlPullParser xrp = Xml.newPullParser();
        xrp.setInput(new FileReader(file));
        eventType = xrp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equalsIgnoreCase("Cube")) {
                        if (xrp.getAttributeCount() == 2) {
                            if (xrp.getAttributeValue(0).equals("USD")) {
                                cambio = Double.parseDouble(xrp.getAttributeValue(1));
                            }
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    break;
            }
            eventType = xrp.next();
        }
        return cambio;
    }


    public static List<Score> getResults(File file) throws IOException, XmlPullParserException {
        List<Score> list = new ArrayList<>();

        int eventType;
        XmlPullParser xrp = Xml.newPullParser();
        xrp.setInput(new FileReader(file));
        eventType = xrp.getEventType();

        while ((eventType = xrp.next()) != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equalsIgnoreCase("g")) {
                        Map<String, String> attributes = getAttributes(xrp);
                        Team local = new Team(attributes.get("h"), attributes.get("hs"));
                        Team visit = new Team(attributes.get("v"), attributes.get("vs"));
                        list.add(new Score(local, visit));
                    }
                    break;
                default:
                    break;
            }
        }
        return list;
    }

    private static Map<String, String> getAttributes(XmlPullParser parser) {
        Map<String, String> attrs = null;
        int acount = parser.getAttributeCount();
        if (acount != -1) {
            attrs = new HashMap<String, String>(acount);
            for (int x = 0; x < acount; x++) {
                attrs.put(parser.getAttributeName(x), parser.getAttributeValue(x));
            }
        } else {
            return null;
        }
        return attrs;
    }

    public static String getDate(File file) throws IOException, XmlPullParserException {
        String res = "";

        int eventType;
        XmlPullParser xrp = Xml.newPullParser();
        xrp.setInput(new FileReader(file));
        eventType = xrp.getEventType();

        while ((eventType = xrp.next()) != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xrp.getName().equalsIgnoreCase("gms")) {
                        Map<String, String> attributes = getAttributes(xrp);
                        res += "Week: " + attributes.get("w");
                        res += " Year: " + attributes.get("y");
                    }
                    break;
                default:
                    break;
            }
        }
        return res;
    }

}
