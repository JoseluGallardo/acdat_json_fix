package com.jsw.acdat_json_fix;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Ejercicio4Activity extends ListActivity {

    ImageView ivLogo;
    TextView tvDate;
    ScoreAdapter mAdapter;
    AsyncHttpClient mClient;
    Context mContext;
    List<Score> mScoreList;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);
        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        tvDate = (TextView) findViewById(R.id.tv_date);
        this.mContext = this;
        refreshData();
    }

    private void refreshData() {
        this.mListView = this.getListView();
        String WEB = "http://www.nfl.com/liveupdate/scorestrip/ss.xml";
        final ProgressDialog progreso = new ProgressDialog(mContext);
        mClient = new AsyncHttpClient();

        final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "scores.txt");
        RequestHandle requestHandle = mClient.get(WEB, new FileAsyncHttpResponseHandler(miFichero) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    //Se almacena en una variable de tipo 'double' el valor de cambio de euros a dólares
                    mScoreList = Utils.getResults(miFichero);
                    mAdapter = new ScoreAdapter(mContext, mScoreList);
                    mListView.setAdapter(mAdapter);
                    ivLogo.setImageResource(R.drawable.header);
                    tvDate.setText(Utils.getDate(miFichero));
                } catch (IOException | XmlPullParserException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Obteniendo Resultados . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
        requestHandle.setTag("");
    }
}
