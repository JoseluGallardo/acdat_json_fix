package com.jsw.acdat_json_fix;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void ejercicio1(View v) {
        startActivity(new Intent(this, Ejercicio1Activity.class));
    }

    public void ejercicio2(View v) {
        startActivity(new Intent(this, Ejercicio2Activity.class));
    }

    public void ejercicio3(View v) {
        startActivity(new Intent(this, Ejercicio3Activity.class));
    }

    public void ejercicio4(View v) {
        startActivity(new Intent(this, Ejercicio4Activity.class));
    }
}
