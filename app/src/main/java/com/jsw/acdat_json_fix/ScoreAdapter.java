package com.jsw.acdat_json_fix;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Joselu on 7/1/17.
 */

public class ScoreAdapter extends ArrayAdapter<Score> {

    private Context context;

    public ScoreAdapter(Context context, List<Score> results) {
        super(context, R.layout.scoreboard);
        this.addAll(results);
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View item = convertView;
        TeamHolder teamHolder;

        if (item == null) {
            //1.crear un objeto inflater que incicializamos al Layout inflater del contexto
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //2.inflar la vista . crear en memoria el objeto view que contienen los widgets
            item = inflater.inflate(R.layout.scoreboard, null);

            //asignar memoria al holder
            teamHolder = new TeamHolder();

            //3.vinculado con el item(view), que se acaba de crear, con las variables del holder
            //con las direcciones de memoria de lo que se esta visualizando
            teamHolder.imgLocal = (ImageView) item.findViewById(R.id.iv_home);
            teamHolder.imgVisitor = (ImageView) item.findViewById(R.id.iv_visitor);
            teamHolder.localName = (TextView) item.findViewById(R.id.tv_home);
            teamHolder.visitorName = (TextView) item.findViewById(R.id.tv_visitor);
            teamHolder.localScore = (TextView) item.findViewById(R.id.tv_score_home);
            teamHolder.visitorScore = (TextView) item.findViewById(R.id.tv_score_visit);

            item.setTag(teamHolder);

        } else {
            //en caso de que se tenga un objeto se reutiliza
            teamHolder = (TeamHolder) item.getTag();
        }

        //4 asignar datos del adapter a los widget
        Picasso.with(context).load(getItem(position).home.image).into(teamHolder.imgLocal);
        Picasso.with(context).load(getItem(position).visitor.image).into(teamHolder.imgVisitor);
        teamHolder.localName.setText(getItem(position).home.name);
        teamHolder.localScore.setText(getItem(position).home.score);
        teamHolder.visitorName.setText(getItem(position).visitor.name);
        teamHolder.visitorScore.setText(getItem(position).visitor.score);

        return item;
    }

    /**
     * Clase interna con la memoria hecha
     */
    class TeamHolder {
        ImageView imgLocal, imgVisitor;
        TextView localName, visitorName, localScore, visitorScore;

    }
}
